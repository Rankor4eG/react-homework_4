import { useEffect,useState  } from 'react';
import { Route, Routes  } from 'react-router-dom';
import { useDispatch,useSelector } from 'react-redux';
import Header from './components/Header';
import HomePage from './components/HomePage/HomePage';
import CartPage from './pages/CartPage/CartPage';
import FavouritePage from './pages/FavouritePage/FavouritePage';
import { actionFetchProducts } from './reducers/products.reducer';
import { selectProducts } from './selectors';
import './App.scss';


const App = () =>{

const dispatch = useDispatch();

const [cart,setCart] = useState(localStorage.getItem('cart')  ? JSON.parse(localStorage.getItem('cart')) : [])
const [favourite,setFavourite] = useState(localStorage.getItem('fav') ? JSON.parse(localStorage.getItem('fav')) : [])



const products = useSelector(selectProducts);

useEffect(()=>{
  dispatch(actionFetchProducts())
  
},[dispatch])


const addToCart=(id)=>{
 
    setCart(
       [...cart,id]
    )
 
  localStorage.setItem('cart', JSON.stringify([...cart, id]))
}
const removeFromCart =(id)=>{
 setCart(
    
    cart.filter((cartItem)=> cartItem !== id)
  )
  localStorage.setItem('cart', JSON.stringify(cart.filter((cartItem)=> cartItem !== id)))
}
const addToFavourite = (id) => {
  setFavourite(
    [...favourite, id]
  )
  localStorage.setItem('fav', JSON.stringify([...favourite, id]))
}
 const removeFromFavourite = (id) => {
  setFavourite(
    
    favourite.filter((fav) => fav !== id)
  )
  localStorage.setItem('fav', JSON.stringify(favourite.filter((fav) => fav !== id)))


}

const sendForm = (values) =>{
  const userInformation = `User information:
    First name: ${values.userFirstName}
    Last name: ${values.userLastName}
    Age: ${values.userAge}
    Delivery address: ${values.address}
    Phone number: ${values.phoneNumber} `
    console.log(userInformation)
    cart.forEach((item)=>{
        console.log(item)
    })
    setCart([])
    localStorage.removeItem('cart')
}

return(
  <>
     <div className="App">
          <Header cartCount={cart.length} favCount={favourite.length}/>
          <Routes>
            <Route path='/'  element={
              <HomePage
              products = {products}
                    addToCart={addToCart} 
                    removeFromCart={removeFromCart} 
                    cart ={cart} 
                    favourite={favourite}
                    addToFavourite = {addToFavourite}
                    removeFromFavourite = {removeFromFavourite}/>
                  } 
                />
                <Route path='/home' element={
                    <HomePage
                    products= {products}
                      addToCart={addToCart} 
                      removeFromCart={removeFromCart} 
                      cart ={cart} 
                      favourite={favourite}
                      addToFavourite = {addToFavourite}
                      removeFromFavourite = {removeFromFavourite}/> 
                      } 
                  />
                  <Route path='/cart' element={
                      <CartPage 
                      removeFromCart={removeFromCart} 
                      cartItemId={cart} 
                      handleSubmit={sendForm}
                    />} 
                  />
                  <Route path='/favourite' element={
                        <FavouritePage
                          cartItem={cart}
                          products= {products}
                          addToCart={addToCart} 
                          removeFromCart={removeFromCart}  
                          favourite={favourite}
                          addToFavourite = {addToFavourite}
                          removeFromFavourite = {removeFromFavourite}
                        />
                  
                  } />
          </Routes>
          
    </div>
    
  </>
)
}

export default App;


