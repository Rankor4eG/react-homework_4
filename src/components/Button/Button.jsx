
import '../Button/Button.scss'



const Button = ({text,backgroundColor,onClick,className,type}) =>{

    return(<button className={className} type={type} style={{backgroundColor}} onClick={onClick}>{text}</button>)
}

Button.defaultProps ={
    type : 'button',
}
export default Button;