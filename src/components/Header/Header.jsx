import React, {Component} from "react";
import '../Header/Header.scss'
import { Link } from 'react-router-dom';
import Cart from "./components/Cart";
import Favourite from "./components/Favourite";

class Header extends Component{
    render(){
        const {cartCount,favCount} = this.props
        return(
            <header className="header">
                <div className="header-wrap">
                    <div className="header-left">
                        <Link to="/home" className="homeLogo">
                            <h2>HOME</h2>
                        </Link>
                    </div>
                    <div className="header-right">
                        <Favourite favCount={favCount}/>
                        <Cart cartCount={cartCount}/>
                    </div>
                </div>
            </header>
            
        )
    }
}
export default Header