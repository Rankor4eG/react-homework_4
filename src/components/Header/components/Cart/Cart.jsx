import { Link } from 'react-router-dom';
import '../Cart/Cart.scss'



const Cart = ({cartCount}) =>{
    return(
        <>
            <Link to='/cart' className="cart">
                <img className="cart__icon" src="/img/cart.png" alt="cart" />
                <p className="cart__text">Cart</p>
                <p className="cart__count">({cartCount})</p>
            </Link>
        </>
    )
}


export default Cart;