
import '../Products/Products.scss'
import Card from "./components/Card/Card";




const Products = ({products,addToCart,removeFromCart,cart,favourite,addToFavourite,removeFromFavourite}) =>{


const product = products.map(card =>{
    const isCardInCart = cart.includes(card)
    const isCardInFavourite = favourite.includes(card)
   
   return <Card 
               addToCart={addToCart}
               removeFromCart = {removeFromCart}  
               cart={cart} 
               product ={card}
               isCardInCart = {isCardInCart}
               key={card.id}
               favourite= {favourite}
               addToFavourite = {addToFavourite}
               removeFromFavourite = {removeFromFavourite}
               isCardInFavourite = {isCardInFavourite}
               />
               
})

return(
    <>
    <div className="products">
                <div className="products__container">
                    <h2 className="products__title">PRODUCTS CURRENTLY ON SALE</h2>
                    <div className="products__wrap">
                        {product}
                    </div>
                    
                </div>
                
                
            </div>
        
    </>
)
}
export default Products;