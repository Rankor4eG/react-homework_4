import '../Card/Card.scss'
import Button from "../../../Button";
import Modal from "../../../Modal";
import {useDispatch, useSelector} from 'react-redux'
import {actionOpenModalAddToCart,actionCloseModalAddToCart} from '../../../../reducers'
import {selectModalCloseAddFromCart} from '../../../../selectors'

const Card = ({isCardInCart,isCardInFavourite,removeFromCart,addToCart,product,removeFromFavourite,addToFavourite})=>{

   
   const dispatch = useDispatch()

    const modalCart = useSelector(selectModalCloseAddFromCart)
        
     
    const showCartModal =()=>{
        dispatch(actionOpenModalAddToCart())
    }
    const closeCartModal =()=>{
        dispatch(actionCloseModalAddToCart())
        }


const addCart = () =>{
    if(isCardInCart){
        removeFromCart(product)
    }else{
        addToCart(product)}
    }

 const addFavourite = () =>{
    if(isCardInFavourite){
        removeFromFavourite(product)
    }else{
        addToFavourite(product)
    }
    
 }
    const url = product.url;
    const id = product.id
    const title = product.title
    const color = product.color
    const price = product.price
   
    const actionsModalCart =
        <div className="modal__footer-actions">
          <Button text='add' onClick={addCart} className='button__modal-cart--footer'/>
          <Button text='cancel'onClick={closeCartModal} className='button__modal-cart--footer'/> 
          </div>

return(
    <>
 <div className="card">
                <div className="card__header">
                    <img className="card__header-img" src={url} alt="img" />
                    <div className="favourite__icon-wrapper" onClick={addFavourite}>
                        
                        <img className="favourite__icon" 
                            src={isCardInFavourite ?'/img/star-active.png' :'/img/star.png'  } 
                             alt="star" 
                            />
                        
                    </div>
                    
                </div>
                <div className="card__content">
                    <p className="card__content-title">{title}</p>
                    <p className="card__content-color">Color: {color}</p>
                    <p className="card__content-article">Product id: {id}</p>

                </div>
                <div className="card__footer">
                    <p className="card__footer-price">{price}</p>
                    <Button className='card__footer-button' onClick={showCartModal}  text = 'add to cart'/>
                    {modalCart &&
                        <Modal
                            header='Do you want to add this product to the cart?'
                            text='This product will be added to the cart!'
                            btnClose={closeCartModal}
                            actions={actionsModalCart}
                        />
                    
                    }
                </div>
            </div>
    </>
)
}

export default Card;

