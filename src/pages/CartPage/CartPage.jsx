import CartItem from './components/CartItem/CartItem'
import '../CartPage/CartPage.scss'
import FormOfOrdering from "../../components/Form/Form";

const CartPage = ({cartItemId,removeFromCart, handleSubmit}) =>{


const productInCart = cartItemId.map(inCarts =>{
        return (
            <CartItem 
                title={inCarts.title} 
                url={inCarts.url} 
                color={inCarts.color} 
                oncklick={()=>{console.log('delete');}}
                key = {inCarts.id} 
                id={inCarts.id}
                price={inCarts.price}
                removeFromCart={removeFromCart}
                product={inCarts}
            />
        )
})

    return(

        <>
            {productInCart.length > 0 ? (
                <>
                    <div className='cart-page'>
                <h2 className="cart-page__title">{productInCart.length} CARS IN CART</h2>
                <div className='cart-page__container' >
                    {productInCart}
                </div>
                <FormOfOrdering handleSubmit={handleSubmit}/>
            </div>
                </>
                    )
                            :
                    (<div className='empty'>
                        <h3>YOUR SHOPPING CART IS EMPTY!</h3>
                        <p>Look at our catalog and choose the products that interest you.</p>
                    </div>)
            }
        </>
    )
}

export default CartPage;





