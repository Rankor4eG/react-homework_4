
import Button from "../../../../components/Button";
import Modal from '../../../../components/Modal'
import '../CartItem/CartItem.scss'
import {useDispatch, useSelector} from 'react-redux'
import {selectModalCloseDeleteFromCart} from '../../../../selectors'
import {actionModalOpenDeleteFromCart,actionModalCloseDeleteFromCart} from '../../../../reducers'



const CartItem = ({product,title,url,color,id,price,removeFromCart}) =>{
   

    const dispatch = useDispatch()
    const modalDelete = useSelector(selectModalCloseDeleteFromCart)
    
const deleteItem = () =>{
     removeFromCart(product)
 
    
}
  const actions = 
  <div className="modal__footer">
        <Button className='button__modal-cart--footer' onClick={deleteItem} text='Delete'/>
        <Button className='button__modal-cart--footer' onClick={()=>{dispatch(actionModalCloseDeleteFromCart())}} text='Cancel'/>
  </div>

  const onBuyCars = () => {
    alert("Do you want to buy this car?")
  }

    return(
        <>
        <div className="cart-item__wrap">
            <div className="cart-item__img">
                <img src={url} alt={title} />
            </div>
            <div className="cart-item__content">
                <h2 className="cart-item__content-title">{title}</h2>
                <p className="cart-item__content-color">Color: {color}</p>
                <p className="cart-item__content-id">Product id: {id}</p>
            </div>
            <div className="cart-item__buy">
                <div className="cart-item__buy-wrapper">
                        <Button className='cart-item__buy-button--close' onClick={()=>{dispatch(actionModalOpenDeleteFromCart())}} text ='X'/>
                        {modalDelete && 
                        <Modal 
                        header='Do you want delete this product?'
                        text='This product will be delete from the cart.' 
                        btnClose={()=>{dispatch(actionModalCloseDeleteFromCart())}} 
                        actions={actions}/>
                        }
                        <p className="cart-item__buy-price">{price}</p>
                        <Button className='cart-item__buy-button--buy' onClick={() => onBuyCars()}  style = {{background : 'green'}} text='Buy Now'/>
                </div>
            </div>
        </div>
        </>
    )
}

export default CartItem;