import FavouriteItem from './components/FavouriteItem/FavouriteItem'
import '../FavouritePage/FavouritePage.scss'

const FavouritePage = ({addToCart,favourite,removeFromFavourite}) =>{

const itemInFavourite = favourite.map(inFavourite =>{
    
   return (<FavouriteItem title={inFavourite.title} url={inFavourite.url} color={inFavourite.color} addToCart={addToCart} id={inFavourite.id}
                            key={inFavourite.id} removeFromFavourite={removeFromFavourite} inFavourite={inFavourite} price={inFavourite.price}
                             />)
})

    return(
        <>
        <div className='favourite-page'>
            <h2 className="favourite-page__title">CARS FAVOURITE</h2>
            <div className='favourite-page__container'>
                <div className="favourite-page__wrap">
                    {itemInFavourite}
                </div>
            </div>
                
        </div>
        </>
    )
}

export default FavouritePage;
