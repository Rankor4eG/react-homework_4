
import Modal from '../../../../components/Modal'
import Button from '../../../../components/Button'
import '../FavouriteItem/FavouriteItem.scss'
import {useDispatch, useSelector} from 'react-redux'
import {actionOpenModalAddToCart,actionCloseModalAddToCart,actionOpenModalDeleteFromFav,actionCloseModalDeleteFromFav} from '../../../../reducers'
import {selectModalAddToCart,selectModalDeleteFromFav} from '../../../../selectors'
const  FavouriteItem = ({title,url,color,id,price,removeFromFavourite,inFavourite,addToCart}) =>{
    

   
    
    const dispatch = useDispatch()
   
    const modalDelFav = useSelector(selectModalDeleteFromFav)
    const modalAddCart = useSelector(selectModalAddToCart)
    
    const deleteFromFav = () =>{
        removeFromFavourite(inFavourite)
    }
    const addCart = () =>{
        addToCart(inFavourite)
    }
    const actionsCart = 
    <div className="modal__footer">
          <Button className='button__modal-cart--footer' onClick={addCart} text='ADD'/>
          <Button className='button__modal-cart--footer' onClick={()=>{dispatch(actionCloseModalAddToCart())}} text='CANCEL'/>
    </div>
  const actionsFav = 
  <div className="modal__footer">
        <Button className='button__modal-cart--footer' onClick={deleteFromFav} text='DELETE'/>
        <Button className='button__modal-cart--footer' onClick={()=>{dispatch(actionCloseModalDeleteFromFav())}} text='CANCEL'/>
  </div>

    return (
    
    <>
    <div className="card">
                <div className="card__header">
                    <img className="card__header-img" src={url} alt="img" />
                    <div className="favourite__icon-wrapper">
                        
                        <img className="favourite__icon" 
                            src={'/img/star-active.png'} 
                            alt="star" 
                            onClick={()=>{dispatch(actionOpenModalDeleteFromFav())}}
                            />
                            {modalDelFav && 
                                <Modal
                                    header='Do you want delete this product from favourite?'
                                    text='This product will be added to the favorite!'
                                    btnClose={()=>{dispatch(actionCloseModalDeleteFromFav())}}
                                    actions={actionsFav}
                                />
                            }
                    </div>
                </div>
                    
                
                <div className="favourite-item__content">
                    <p className="favourite-item__content-title">{title}</p>
                    <p className="favourite-item__content-color">Color: {color}</p>
                    <p className="favourite-item__content-article">Product id: {id}</p>

                </div>
                <div className="favourite-item__footer">
                    <p className="favourite-item__footer-price">{price}</p>
                    <Button className='favourite-item__footer-button' onClick={()=>{dispatch(actionOpenModalAddToCart())}}  text = 'add to cart'/>
                    {modalAddCart &&
                        <Modal
                            header='Do you want to add this product to the cart?'
                            text='This product will be added to the cart!'
                            btnClose={()=>{dispatch(actionCloseModalAddToCart())}}
                            actions={actionsCart}
                        />
                    
                    }
                </div>
            </div>
    </>
    
    )
}
export default FavouriteItem;