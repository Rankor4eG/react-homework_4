
import modalAddToCartReducer, {actionOpenModalAddToCart,actionCloseModalAddToCart} from './modalAddToCart.reducer'
import modalDeleteFromCartReducer,{actionModalOpenDeleteFromCart,actionModalCloseDeleteFromCart} from "./modalDeleteFromCart.reducer";
 
import modalDeleteFromFavReducer, {actionOpenModalDeleteFromFav,actionCloseModalDeleteFromFav} from "./modalDeleteFromFav.reducer";
import productsReducer from './products.reducer';
export {
   
    modalAddToCartReducer,
    modalDeleteFromCartReducer,
    modalDeleteFromFavReducer,
    productsReducer,
    actionOpenModalAddToCart,
    actionCloseModalAddToCart,
    actionModalOpenDeleteFromCart,
    actionModalCloseDeleteFromCart,
    actionOpenModalDeleteFromFav,
    actionCloseModalDeleteFromFav
}

