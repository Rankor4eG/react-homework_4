import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    modalAddToCartIsOpen: false
}

export const modalAddToCartSlice = createSlice({

    name: "modalAddToCart",
    initialState,
    reducers:{
        actionOpenModalAddToCart: (state)=>{
            state.modalAddToCartIsOpen = true
        },
        actionCloseModalAddToCart: (state) =>{
            state.modalAddToCartIsOpen = false
        }
    }
})

export const {actionOpenModalAddToCart,actionCloseModalAddToCart} = modalAddToCartSlice.actions

export default modalAddToCartSlice.reducer