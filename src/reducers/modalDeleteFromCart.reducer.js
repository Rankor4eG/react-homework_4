import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    modalDeleteFromCartIsOpen: false,
}

export const modalDeleteFromCartSlise = createSlice({
    name: "modalDeleteFromCart",
    initialState,
    reducers: {
        actionModalOpenDeleteFromCart: (state) =>{
            state.modalDeleteFromCartIsOpen = true;
        },
        actionModalCloseDeleteFromCart: (state) =>{
            state.modalDeleteFromCartIsOpen = false;
        }
    }
})

export const {actionModalOpenDeleteFromCart,actionModalCloseDeleteFromCart} = modalDeleteFromCartSlise.actions

export default modalDeleteFromCartSlise.reducer