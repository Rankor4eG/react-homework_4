import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    modalDeleteFromFav: false
}

const modalDeleteFromFavSlice = createSlice({
    name: "modalDeleteFromFav",
    initialState,
    reducers: {
        actionOpenModalDeleteFromFav: (state)=>{
            state.modalDeleteFromFav = true
        },
        actionCloseModalDeleteFromFav: (state)=>{
            state.modalDeleteFromFav = false
        }
    }
})

export const {actionOpenModalDeleteFromFav,actionCloseModalDeleteFromFav} = modalDeleteFromFavSlice.actions
export default modalDeleteFromFavSlice.reducer