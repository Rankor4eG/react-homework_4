import {createAsyncThunk,createSlice} from '@reduxjs/toolkit';



export const actionFetchProducts = createAsyncThunk(
    'products/fetchProducts',
    async () =>{
        const fetchData = async ()=>{
            const response = await fetch('/products.json')
            return response.json()
          }
        return await fetchData()
    }
)

const initialState ={
    data: []
}

const productsSlice = createSlice({
    name: 'products',
    initialState,
    extraReducers:(builder)=>{
        builder.addCase(actionFetchProducts.fulfilled, (state, {payload}) => {
            state.data = payload;
        })
    }
})


 export default productsSlice.reducer