export const selectModalCloseAddFromCart = (state) => state.modalAddCart.modalAddToCartIsOpen;
export const selectModalCloseDeleteFromCart = (state) => state.modalDeleteCart.modalDeleteFromCartIsOpen
export const selectModalAddToCart = (state) => state.modalAddCart.modalAddToCartIsOpen
export const selectModalDeleteFromFav = (state) => state.modalDeleteFav.modalDeleteFromFav