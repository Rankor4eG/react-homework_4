import {configureStore} from '@reduxjs/toolkit'
import{modalAddToCartReducer, modalDeleteFromCartReducer,modalDeleteFromFavReducer,productsReducer} from '../reducers'

const store = configureStore({
    reducer:{
        
        modalAddCart: modalAddToCartReducer,
        modalDeleteCart: modalDeleteFromCartReducer,
        modalDeleteFav: modalDeleteFromFavReducer,
        products: productsReducer,
    }
})

export default store;